terraform {
  backend "s3" {
    bucket         = "fibonacci-calculator-devops-tfstate"
    key            = "fibonacci-calculator.tfstate"
    region         = "ap-southeast-2"
    encrypt        = true
    dynamodb_table = "fibonacci-calculator-devops-tfstate-lock"
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
  eks_cluster_name = "${var.prefix}-${terraform.workspace}-eks-cluster"
}

provider "aws" {
  region  = "ap-southeast-2"
  version = "~> 2.54.0"
}

data "aws_region" "current" {}

data "aws_eks_cluster" "eks_cluster_main" {
  name = module.eks.eks_cluster_main.name
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = module.eks.eks_cluster_main.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks_cluster_main.endpoint
  cluster_ca_certificate = base64decode(module.eks.eks_cluster_main.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster_auth.token
  load_config_file       = false
}

module "vpc" {
  source = "./vpc"

  prefix           = local.prefix
  common_tags      = local.common_tags
  eks_cluster_name = local.eks_cluster_name
}

module "eks" {
  source = "./eks"

  prefix                       = local.prefix
  common_tags                  = local.common_tags
  eks_cluster_name             = local.eks_cluster_name
  vpc_id                       = module.vpc.vpc_id
  subnet_ids                   = module.vpc.subnet_ids
  control_plane_security_group = module.vpc.control_plane_security_group
}

module "kubeconfig" {
  source = "./kubeconfig"

  kubectl_user           = var.kubectl_user
  eks_cluster_main       = module.eks.eks_cluster_main
  eks_cluster_endpoint   = module.eks.eks_cluster_endpoint
  node_instance_role_arn = module.nodes.node_instance_role.arn
}

module "nodes" {
  source = "./nodes"

  prefix                       = local.prefix
  common_tags                  = local.common_tags
  bastion_key_name             = var.bastion_key_name
  vpc_id                       = module.vpc.vpc_id
  private_subnet_ids           = module.vpc.private_subnet_ids
  eks_cluster_main             = module.eks.eks_cluster_main
  control_plane_security_group = module.vpc.control_plane_security_group
}

module "redis" {
  source = "./redis"

  worker_node_group = module.nodes.worker_node_group
}

module "postgres" {
  source = "./postgres"
}

module "multiworker" {
  source = "./multiworker"

  ecr_image_worker         = var.ecr_image_worker
  worker_node_group        = module.nodes.worker_node_group
  redis_cluster_ip_service = module.redis.redis_cluster_ip_service
}

module "multiclient" {
  source = "./multiclient"

  ecr_image_client = var.ecr_image_client
}

module "multiserver" {
  source = "./multiserver"

  ecr_image_server            = var.ecr_image_server
  redis_cluster_ip_service    = module.redis.redis_cluster_ip_service
  postgres_cluster_ip_service = module.postgres.postgres_cluster_ip_service
}

module "dns" {
  source = "./dns"

  common_tags            = local.common_tags
  dns_zone_name          = var.dns_zone_name
  subdomain              = var.subdomain
  load_balancer_hostname = module.ingress.load_balancer_hostname
}

module "ingress" {
  source = "./ingress"

  subdomain                 = var.subdomain
  dns_zone_name             = var.dns_zone_name
  aws_acm_arn               = module.dns.aws_acm_arn
  aws_vpc_cidr              = module.vpc.aws_vpc_cidr
  eks_cluster_main          = module.eks.eks_cluster_main
  eks_cluster_endpoint      = module.eks.eks_cluster_endpoint
  tls_term_depends_on       = module.kubeconfig.tls_term_depends_on
  client_cluster_ip_service = module.multiclient.client_cluster_ip_service
  server_cluster_ip_service = module.multiserver.server_cluster_ip_service
}