variable "prefix" {}

variable "common_tags" {}

variable "subnet_ids" {}

variable "vpc_id" {}

variable "control_plane_security_group" {}

variable "eks_cluster_name" {}