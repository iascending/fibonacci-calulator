output "eks_cluster_role_arn" {
  value = aws_iam_role.eks_cluster_role.arn
}

output "eks_cluster_main" {
  value = aws_eks_cluster.main
}

output "eks_cluster_endpoint" {
  value = aws_eks_cluster.main.endpoint
}