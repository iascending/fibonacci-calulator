data "aws_region" "current" {}

# Create IAM role for EKS cluster
resource "aws_iam_role" "eks_cluster_role" {
  name               = "${var.prefix}-eks-cluster-role"
  assume_role_policy = file("./templates/eks/assume-role-policy.json")

  tags = var.common_tags
}

resource "aws_iam_policy" "AmazonEKSClusterCloudWatchMetricsPolicy" {
  name        = "AmazonEKSClusterCloudWatchMetricsPolicy"
  path        = "/"
  description = "Allow EKS to write cloudwatch logs"
  policy      = file("./templates/eks/cloudwatch-write-policy.json")
}

resource "aws_iam_policy" "AmazonEKSClusterNLBPolicy" {
  name        = "AmazonEKSClusterNLBPolicy"
  path        = "/"
  description = "Allow EKS to create NLB"
  policy      = file("./templates/eks/eks-NLB-policy.json")
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_iam_role_policy_attachment" "AmazonEKSCloudWatchMetricsPolicy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = aws_iam_policy.AmazonEKSClusterCloudWatchMetricsPolicy.arn
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterNLBPolicy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = aws_iam_policy.AmazonEKSClusterNLBPolicy.arn
}

resource "aws_cloudwatch_log_group" "eks_cluster_logs" {
  name = "${var.eks_cluster_name}-log-group"

  tags = var.common_tags
}

# Create EKS cluster
resource "aws_eks_cluster" "main" {
  name     = var.eks_cluster_name
  role_arn = aws_iam_role.eks_cluster_role.arn

  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  vpc_config {
    security_group_ids      = [var.control_plane_security_group]
    subnet_ids              = var.subnet_ids
    endpoint_private_access = true
    endpoint_public_access  = true
  }

  timeouts {
    delete = "30m"
  }

  depends_on = [
    aws_cloudwatch_log_group.eks_cluster_logs,
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSCloudWatchMetricsPolicy,
    aws_iam_role_policy_attachment.AmazonEKSClusterNLBPolicy,
    aws_iam_role.eks_cluster_role
  ]
}
