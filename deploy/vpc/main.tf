data "aws_region" "current" {}

resource "aws_vpc" "main" {
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-vpc")
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-main")
  )
}

#####################################################
# Public Subnet - Inbound/Outbound Internet Access  #
#####################################################

resource "aws_subnet" "public_a" {
  cidr_block              = "192.168.0.0/18"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}a"

  tags = merge(
    var.common_tags,
    {
      "Name"                                          = "${var.prefix}-public-a",
      "kubernetes.io/role/elb"                        = 1,
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    }
  )
}

resource "aws_route_table" "public_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.common_tags,
    {
      "Name"    = "${var.prefix}-public-a",
      "Network" = "Public"
    }
  )
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_a.id
}

resource "aws_route" "public_internet_access_a" {
  route_table_id         = aws_route_table.public_a.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_eip" "public_a" {
  vpc = true

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-public-a")
  )
}

resource "aws_nat_gateway" "public_a" {
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-public-a")
  )
}

resource "aws_subnet" "public_b" {
  cidr_block              = "192.168.64.0/18"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    var.common_tags,
    {
      "Name"                                          = "${var.prefix}-public-a",
      "kubernetes.io/role/elb"                        = 1,
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    }
  )
}

resource "aws_route_table" "public_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.common_tags,
    {
      "Name"    = "${var.prefix}-public-a",
      "Network" = "Public"
    }
  )
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}

resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_eip" "public_b" {
  vpc = true

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-public-b")
  )
}

resource "aws_nat_gateway" "public_b" {
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-public-b")
  )
}

###################################################
# Private Subnets - Outbound internet access only #
###################################################
resource "aws_subnet" "private_a" {
  cidr_block = "192.168.128.0/18"
  vpc_id     = aws_vpc.main.id

  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    var.common_tags,
    {
      "Name"                                          = "${var.prefix}-private-a",
      "kubernetes.io/role/internal-elb"               = 1,
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    }
  )
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-private-a")
  )
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id         = aws_route_table.private_a.id
  nat_gateway_id         = aws_nat_gateway.public_a.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_subnet" "private_b" {
  cidr_block = "192.168.192.0/18"
  vpc_id     = aws_vpc.main.id

  availability_zone = "${data.aws_region.current.name}b"

  tags = merge(
    var.common_tags,
    {
      "Name"                                          = "${var.prefix}-private-a",
      "kubernetes.io/role/internal-elb"               = 1,
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    }
  )
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-private-b")
  )
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}

resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private_b.id
  nat_gateway_id         = aws_nat_gateway.public_b.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_security_group" "control_plane" {
  description = "Security group for the cluster control plane communication with worker nodes"
  name        = "${var.prefix}-control-plane"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
      aws_subnet.public_a.cidr_block,
      aws_subnet.public_b.cidr_block,
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  tags = merge(
    var.common_tags,
    map("Name", "${var.prefix}-control-plane-security-group")
  )
}
