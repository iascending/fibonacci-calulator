resource "kubernetes_service" "redis_cluster_ip_service" {
  metadata {
    name = "redis-cluster-ip-service"
  }

  spec {
    type = "ClusterIP"
    selector = {
      component = "redis"
    }
    port {
      port        = 6379
      target_port = 6379
    }
  }
  depends_on = [
    kubernetes_deployment.redis_deployment
  ]
}

resource "kubernetes_deployment" "redis_deployment" {
  metadata {
    name = "redis-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "redis"
      }
    }

    template {
      metadata {
        labels = {
          component = "redis"
        }
      }
      spec {
        container {
          name  = "redis"
          image = "redis"
          port {
            container_port = 6379
          }
        }
      }
    }
  }
  depends_on = [
    var.worker_node_group
  ]
}