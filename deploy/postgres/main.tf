resource "kubernetes_service" "postgres_cluster_ip_service" {
  metadata {
    name = "postgres-cluster-ip-service"
  }

  spec {
    type = "ClusterIP"
    selector = {
      component = "postgres"
    }
    port {
      port        = 5432
      target_port = 5432
    }
  }
  depends_on = [
    kubernetes_deployment.postgres_deployment
  ]
}

resource "kubernetes_persistent_volume_claim" "database_persistent_volume_claim" {
  metadata {
    name = "database-persistent-volume-claim"
  }

  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "2Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "postgres_deployment" {
  metadata {
    name = "postgres-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "postgres"
      }
    }

    template {
      metadata {
        labels = {
          component = "postgres"
        }
      }
      spec {
        volume {
          name = "postgres-storage"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.database_persistent_volume_claim.metadata.0.name
          }
        }
        container {
          name  = "postgres"
          image = "postgres"

          port {
            container_port = 5432
          }

          volume_mount {
            name       = "postgres-storage"
            mount_path = "/var/lib/postgresql/data"
            sub_path   = "postgres"
          }

          env {
            name  = "PGPASSWORD"
            value = "pgpassword"
          }
        }
      }
    }
  }
}