variable "prefix" {
  default = "fica"
}

variable "project" {
  default = "fibonacci-calculator"
}

variable "contact" {
  default = "iascending@gmail.com"
}

variable "kubectl_user" {
  description = "IAM user which assumes a role to access kubernetes"
  default     = ""
}

variable "bastion_key_name" {
  default = "Ec2Instance"
}

variable "ecr_image_client" {
  description = "ECR image for Client"
  default     = "760637161969.dkr.ecr.ap-southeast-2.amazonaws.com/fibonacci-caluator-client:latest"
}

variable "ecr_image_server" {
  description = "ECR image for Server"
  default     = "760637161969.dkr.ecr.ap-southeast-2.amazonaws.com/fibonacci-calculator-server:latest"
}

variable "ecr_image_worker" {
  description = "ECR image for Worker"
  default     = "760637161969.dkr.ecr.ap-southeast-2.amazonaws.com/fibonacci-calculator-worker:latest"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "lifeascending.net"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "www"
    staging    = "staging"
    dev        = "dev"
    default    = "www"
  }
}
