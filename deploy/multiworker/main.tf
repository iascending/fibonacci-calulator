resource "kubernetes_deployment" "worker_deployment" {
  metadata {
    name = "worker-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "worker"
      }
    }

    template {
      metadata {
        labels = {
          component = "worker"
        }
      }
      spec {
        container {
          name  = "worker"
          image = var.ecr_image_worker

          env {
            name  = "REDIS_HOST"
            value = var.redis_cluster_ip_service.metadata.0.name
          }
          env {
            name  = "REDIS_PORT"
            value = "6379"
          }
        }
      }
    }

  }
}
