resource "kubernetes_service" "server_cluster_ip_service" {
  metadata {
    name = "server-cluster-ip-service"
  }

  spec {
    type = "ClusterIP"
    selector = {
      component = "server"
    }
    port {
      port        = 5000
      target_port = 5000
    }
  }
  depends_on = [
    kubernetes_deployment.server_deployment
  ]
}

resource "kubernetes_deployment" "server_deployment" {
  metadata {
    name = "server-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "server"
      }
    }

    template {
      metadata {
        labels = {
          component = "server"
        }
      }
      spec {
        container {
          name  = "server"
          image = var.ecr_image_server

          port {
            container_port = 5000
          }

          env {
            name  = "REDIS_HOST"
            value = var.redis_cluster_ip_service.metadata.0.name
          }
          env {
            name  = "REDIS_PORT"
            value = "6379"
          }
          env {
            name  = "PGUSER"
            value = "postgres"
          }
          env {
            name  = "PGHOST"
            value = var.postgres_cluster_ip_service.metadata.0.name
          }
          env {
            name  = "PGPORT"
            value = "5432"
          }
          env {
            name  = "PGDATABASE"
            value = "postgres"
          }
          env {
            name  = "PGPASSWORD"
            value = "pgpassword"
          }
        }
      }
    }
  }
}