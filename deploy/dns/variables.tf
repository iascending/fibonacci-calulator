variable "common_tags" {}

variable "dns_zone_name" {}

variable "subdomain" {}

variable "load_balancer_hostname" {}