variable "eks_cluster_main" {}

variable "eks_cluster_endpoint" {}

variable "kubectl_user" {}

variable "node_instance_role_arn" {}

variable "aws_vpc_cidr" {
  description = "The cidr block of AWS EKS cluster"
  default     = "192.168.0.0/16"
}