output "eks_kubectl_role" {
  value = aws_iam_role.eks_kubectl_role
}

output "tls_term_depends_on" {
  value      = {}
  depends_on = [kubernetes_config_map.aws_auth_configmap]
}