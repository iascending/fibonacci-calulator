# Create a IAM role for kubectl to assume
data "aws_iam_policy_document" "kubectl_assume_role_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "AWS"
      identifiers = ["${var.kubectl_user}"]
    }
  }
}

resource "aws_iam_role" "eks_kubectl_role" {
  name               = "${var.eks_cluster_main.name}-kubectl-access-role"
  assume_role_policy = data.aws_iam_policy_document.kubectl_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "eks_kubectl-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_kubectl_role.name
}

resource "aws_iam_role_policy_attachment" "eks_kubectl-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_kubectl_role.name
}

resource "aws_iam_role_policy_attachment" "eks_kubectl-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_kubectl_role.name
}

data "template_file" "kubectl_iam_map" {
  template = file("./templates/kubeconfig/kubectl_iam_map.yml")

  vars = {
    eks_kubectl_role_arn   = aws_iam_role.eks_kubectl_role.arn
    node_instance_role_arn = var.node_instance_role_arn
  }
}

# Enable worker node to register to EKS cluster
resource "kubernetes_config_map" "aws_auth_configmap" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }
  data = {
    mapRoles = data.template_file.kubectl_iam_map.rendered
  }
}
