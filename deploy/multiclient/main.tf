resource "kubernetes_service" "client_cluster_ip_service" {
  metadata {
    name = "client-cluster-ip-service"
  }

  spec {
    type = "ClusterIP"
    selector = {
      component = "web"
    }
    port {
      port        = 3000
      target_port = 3000
    }
  }
  depends_on = [
    kubernetes_deployment.client_deployment
  ]
}

resource "kubernetes_deployment" "client_deployment" {
  metadata {
    name = "client-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "web"
      }
    }

    template {
      metadata {
        labels = {
          component = "web"
        }
      }
      spec {
        container {
          name  = "client"
          image = var.ecr_image_client

          port {
            container_port = 3000
          }
        }
      }
    }
  }
}
