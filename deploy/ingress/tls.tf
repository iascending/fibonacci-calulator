resource "kubernetes_namespace" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"

    labels = {
      "app.kubernetes.io/name"     = "ingress-nginx"
      "app.kubernetes.io/instance" = "ingress-nginx"
    }
  }

  depends_on = [var.tls_term_depends_on]
}

resource "kubernetes_service_account" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }
}

resource "kubernetes_config_map" "ingress_nginx_controller" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  data = {}
}

resource "kubernetes_cluster_role" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"
    # namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps", "endpoints", "nodes", "pods", "secrets"]
    verbs      = ["list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["get"]
  }
  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get", "list", "update", "watch"]
  }
  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["events"]
    verbs      = ["create", "patch"]
  }
  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }
  rule {
    api_groups = ["networking.k8s.io"]
    resources  = ["ingressclasses"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"
    # namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "ingress-nginx"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
  }
}

resource "kubernetes_role" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs      = ["get"]
  }
  rule {
    api_groups = [""]
    resources  = ["configmaps", "pods", "secrets", "endpoints"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get", "list", "update", "watch"]
  }
  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }
  rule {
    api_groups = ["networking.k8s.io"]
    resources  = ["ingressclasses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups     = [""]
    resources      = ["configmaps"]
    resource_names = ["ingress-controller-leader-nginx"]
    verbs          = ["get", "update"]
  }
  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    verbs      = ["create"]
  }
  rule {
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["create", "get", "update"]
  }
  rule {
    api_groups = [""]
    resources  = ["events"]
    verbs      = ["create", "patch"]
  }
}

resource "kubernetes_role_binding" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "ingress-nginx"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "ingress-nginx"
    namespace = "ingress-nginx"
  }
}

resource "kubernetes_service" "ingress_nginx_controller_admission" {
  metadata {
    name      = "ingress-nginx-controller-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  spec {
    type = "ClusterIP"
    port {
      name        = "https-webhook"
      port        = 443
      target_port = "webhook"
    }
    selector = {
      "app.kubernetes.io/name"      = "ingress-nginx"
      "app.kubernetes.io/instance"  = "ingress-nginx"
      "app.kubernetes.io/component" = "controller"
    }
  }
}

resource "kubernetes_service" "ingress_nginx_controller" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    annotations = {
      "service.beta.kubernetes.io/aws-load-balancer-backend-protocol"                  = "http"
      "service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout"           = "60"
      "service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled" = "true"
      "service.beta.kubernetes.io/aws-load-balancer-ssl-cert"                          = var.aws_acm_arn
      "service.beta.kubernetes.io/aws-load-balancer-ssl-ports"                         = "https"
      "service.beta.kubernetes.io/aws-load-balancer-type"                              = "elb"
    }

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  spec {
    type                    = "LoadBalancer"
    external_traffic_policy = "Local"
    port {
      name        = "http"
      port        = 80
      protocol    = "TCP"
      target_port = "tohttps"
    }
    port {
      name        = "https"
      port        = 443
      protocol    = "TCP"
      target_port = "http"
    }

    selector = {
      "app.kubernetes.io/name"      = "ingress-nginx"
      "app.kubernetes.io/instance"  = "ingress-nginx"
      "app.kubernetes.io/component" = "controller"
    }
  }
}

resource "kubernetes_deployment" "ingress_nginx_controller" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "controller"
    }
  }

  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "ingress-nginx"
        "app.kubernetes.io/instance"  = "ingress-nginx"
        "app.kubernetes.io/component" = "controller"
      }
    }
    revision_history_limit = 10
    min_ready_seconds      = 0

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "ingress-nginx"
          "app.kubernetes.io/instance"  = "ingress-nginx"
          "app.kubernetes.io/component" = "controller"
        }
      }
      spec {
        dns_policy = "ClusterFirst"
        container {
          name              = "controller"
          image             = "quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.32.0"
          image_pull_policy = "IfNotPresent"
          lifecycle {
            pre_stop {
              exec {
                command = ["/wait-shutdown"]
              }
            }
          }
          args = [
            "/nginx-ingress-controller",
            "--publish-service=ingress-nginx/ingress-nginx-controller",
            "--election-id=ingress-controller-leader",
            "--ingress-class=nginx",
            "--configmap=ingress-nginx/ingress-nginx-controller",
            "--validating-webhook=:8443",
            "--validating-webhook-certificate=/usr/local/certificates/cert",
            "--validating-webhook-key=/usr/local/certificates/key"
          ]
          security_context {
            capabilities {
              drop = ["ALL"]
              add  = ["NET_BIND_SERVICE"]
            }
            run_as_user                = 101
            allow_privilege_escalation = true
          }
          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                field_path = "metadata.name"
              }
            }
          }
          env {
            name = "POD_NAMESPACE"
            value_from {
              field_ref {
                field_path = "metadata.namespace"
              }
            }
          }
          liveness_probe {
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 10
            period_seconds        = 10
            timeout_seconds       = 1
            success_threshold     = 1
            failure_threshold     = 3
          }
          readiness_probe {
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 10
            period_seconds        = 10
            timeout_seconds       = 1
            success_threshold     = 1
            failure_threshold     = 3
          }
          port {
            name           = "http"
            container_port = 80
            protocol       = "TCP"
          }
          port {
            name           = "https"
            container_port = 80
            protocol       = "TCP"
          }
          port {
            name           = "tohttps"
            container_port = 2443
            protocol       = "TCP"
          }
          port {
            name           = "webhook"
            container_port = 8443
            protocol       = "TCP"
          }
          volume_mount {
            name       = "webhook-cert"
            mount_path = "/usr/local/certificates/"
            read_only  = true
          }
          resources {
            requests {
              cpu    = "100m"
              memory = "90Mi"
            }
          }
        }
        service_account_name             = "ingress-nginx"
        termination_grace_period_seconds = 300
        volume {
          name = "webhook-cert"
          secret {
            secret_name = "ingress-nginx-admission"
          }
        }
      }
    }
  }
}

resource "kubernetes_validating_webhook_configuration" "ingress_nginx_admission" {
  metadata {
    name = "ingress-nginx-admission"

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }

  webhook {
    name                      = "validate.nginx.ingress.kubernetes.io"
    admission_review_versions = ["v1beta1", "v1"]

    rule {
      api_groups   = ["extensions", "networking.k8s.io"]
      api_versions = ["v1beta1"]
      operations   = ["CREATE", "UPDATE"]
      resources    = ["ingresses"]
    }
    failure_policy = "Fail"
    client_config {
      service {
        name      = "ingress-nginx-controller-admission"
        path      = "/extensions/v1beta1/ingresses"
        namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
      }
    }
    side_effects = "None"
  }
}

resource "kubernetes_cluster_role" "ingress_nginx_admission" {
  metadata {
    name = "ingress-nginx-admission"
    annotations = {
      "helm.sh/hook"               = "pre-install,pre-upgrade,post-install,post-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }
    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }

  rule {
    api_groups = ["admissionregistration.k8s.io"]
    resources  = ["validatingwebhookconfigurations"]
    verbs      = ["get", "update"]
  }
}

resource "kubernetes_cluster_role_binding" "ingress_nginx_admission" {
  metadata {
    name = "ingress-nginx-admission"
    annotations = {
      "helm.sh/hook"               = "pre-install,pre-upgrade,post-install,post-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }
    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "ingress-nginx-admission"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "ingress-nginx-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
  }
}

resource "kubernetes_job" "ingress_nginx_admission_create" {
  metadata {
    name = "ingress-nginx-admission-create"
    annotations = {
      "helm.sh/hook"               = "pre-install,pre-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }
    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
  }

  spec {
    template {
      metadata {
        name = "ingress-nginx-admission-create"
        labels = {
          "helm.sh/chart"                = "ingress-nginx-2.1.0"
          "app.kubernetes.io/name"       = "ingress-nginx"
          "app.kubernetes.io/instance"   = "ingress-nginx"
          "app.kubernetes.io/version"    = "0.32.0"
          "app.kubernetes.io/managed-by" = "Helm"
          "app.kubernetes.io/component"  = "admission-webhook"
        }
      }
      spec {
        container {
          name              = "create"
          image             = "jettech/kube-webhook-certgen:v1.2.0"
          image_pull_policy = "IfNotPresent"
          args = [
            "create",
            "--host=ingress-nginx-controller-admission,ingress-nginx-controller-admission.ingress-nginx.svc",
            "--namespace=ingress-nginx",
            "--secret-name=ingress-nginx-admission"
          ]
        }
        restart_policy       = "OnFailure"
        service_account_name = "ingress-nginx-admission"
        security_context {
          run_as_non_root = true
          run_as_user     = 2000
        }
      }
    }
  }
}

resource "kubernetes_job" "ingress_nginx_admission_patch" {
  metadata {
    name = "ingress-nginx-admission-patch"
    annotations = {
      "helm.sh/hook"               = "post-install,post-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }
    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
  }
  spec {
    template {
      metadata {
        name = "ingress-nginx-admission-patch"
        labels = {
          "helm.sh/chart"                = "ingress-nginx-2.1.0"
          "app.kubernetes.io/name"       = "ingress-nginx"
          "app.kubernetes.io/instance"   = "ingress-nginx"
          "app.kubernetes.io/version"    = "0.32.0"
          "app.kubernetes.io/managed-by" = "Helm"
          "app.kubernetes.io/component"  = "admission-webhook"
        }
      }
      spec {
        container {
          name              = "patch"
          image             = "jettech/kube-webhook-certgen:v1.2.0"
          image_pull_policy = "IfNotPresent"
          args = [
            "patch",
            "--webhook-name=ingress-nginx-admission",
            "--namespace=ingress-nginx",
            "--patch-mutating=false",
            "--secret-name=ingress-nginx-admission",
            "--patch-failure-policy=Fail"
          ]
        }
        restart_policy       = "OnFailure"
        service_account_name = "ingress-nginx-admission"
        security_context {
          run_as_non_root = true
          run_as_user     = 2000
        }
      }
    }
  }
}

resource "kubernetes_role" "ingress_nginx_admission" {
  metadata {
    name      = "ingress-nginx-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }

  rule {
    api_groups = [""]
    resources  = ["secrets"]
    verbs      = ["get", "create"]
  }
}

resource "kubernetes_role_binding" "ingress_nginx_admission" {
  metadata {
    name      = "ingress-nginx-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    annotations = {
      "helm.sh/hook"               = "pre-install,pre-upgrade,post-install,post-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "ingress-nginx-admission"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "ingress-nginx-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
  }
}

resource "kubernetes_service_account" "ingress_nginx_admission" {
  metadata {
    name      = "ingress-nginx-admission"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name

    annotations = {
      "helm.sh/hook"               = "pre-install,pre-upgrade,post-install,post-upgrade"
      "helm.sh/hook-delete-policy" = "before-hook-creation,hook-succeeded"
    }

    labels = {
      "helm.sh/chart"                = "ingress-nginx-2.1.0"
      "app.kubernetes.io/name"       = "ingress-nginx"
      "app.kubernetes.io/instance"   = "ingress-nginx"
      "app.kubernetes.io/version"    = "0.32.0"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/component"  = "admission-webhook"
    }
  }
}