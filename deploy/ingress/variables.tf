variable "subdomain" {}

variable "dns_zone_name" {}

variable "aws_vpc_cidr" {}

variable "aws_acm_arn" {}

variable "eks_cluster_main" {}

variable "eks_cluster_endpoint" {}

variable "tls_term_depends_on" {}

variable "client_cluster_ip_service" {}

variable "server_cluster_ip_service" {}