data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

resource "kubernetes_ingress" "ingress_service" {
  metadata {
    name = "ingress-service"

    annotations = {
      "kubernetes.io/ingress.class"                       = "nginx"
      "nginx.ingress.kubernetes.io/rewrite-target"        = "/"
      "service.beta.kubernetes.io/aws-load-balancer-type" = "nlb"
      "nginx.ingress.kubernetes.io/ssl-redirect"          = true
    }
  }
  spec {
    rule {
      host = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
      http {
        path {
          path = "/"
          backend {
            service_name = var.client_cluster_ip_service.metadata.0.name
            service_port = 3000
          }
        }

        path {
          path = "/api/"
          backend {
            service_name = var.server_cluster_ip_service.metadata.0.name
            service_port = 5000
          }
        }
      }
    }
  }
}
