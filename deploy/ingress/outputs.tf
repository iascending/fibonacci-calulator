output "load_balancer_hostname" {
  value = kubernetes_ingress.ingress_service.load_balancer_ingress.0.hostname
}