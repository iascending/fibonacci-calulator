# Create IAM role for worker nodes
resource "aws_iam_role" "node_instance_role" {
  name               = "${var.prefix}-node-instance-role"
  assume_role_policy = file("./templates/nodes/assume-role-policy.json")

  tags = var.common_tags
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.node_instance_role.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.node_instance_role.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.node_instance_role.name
}

resource "aws_iam_instance_profile" "node_instance_profile" {
  name = "${var.prefix}-node-instance-profile"
  role = aws_iam_role.node_instance_role.name
}

resource "aws_security_group" "node_security_group" {
  name        = "${var.prefix}-node-security-group"
  description = "Security group for all nodes in the cluster"
  vpc_id      = var.vpc_id

  tags = merge(
    var.common_tags,
    {
      "Name"                                               = "${var.prefix}-node-security-group"
      "kubernetes.io/cluster/${var.eks_cluster_main.name}" = "owned"
    }
  )
}

resource "aws_eks_node_group" "standard_worker_group" {
  cluster_name    = var.eks_cluster_main.name
  node_group_name = "${var.prefix}-standard-worker-group"
  node_role_arn   = aws_iam_role.node_instance_role.arn
  subnet_ids      = var.private_subnet_ids

  scaling_config {
    desired_size = 8
    max_size     = 10
    min_size     = 1
  }

  disk_size      = var.worker_node_ebs_size
  instance_types = [var.worker_node_instance_type]

  remote_access {
    ec2_ssh_key = var.bastion_key_name
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]

  tags = merge(
    var.common_tags,
    map("Name", "${var.eks_cluster_main.name}-standard-worker-node")
  )
}