output "node_instance_role" {
  value = aws_iam_role.node_instance_role
}

output "worker_node_group" {
  value = aws_eks_node_group.standard_worker_group.node_group_name
}