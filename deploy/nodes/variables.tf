variable "prefix" {}

variable "common_tags" {}

variable "private_subnet_ids" {}

variable "eks_cluster_main" {}

variable "vpc_id" {}

variable "control_plane_security_group" {}

variable "bastion_key_name" {}

variable "worker_node_ebs_size" {
  default = 20
}

variable "worker_node_instance_type" {
  default = "t2.micro"
}