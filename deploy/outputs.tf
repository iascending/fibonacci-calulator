output "eks_cluster_role_arn" {
  value = module.eks.eks_cluster_role_arn
}

output "eks_cluster_main" {
  value = module.eks.eks_cluster_main
}

output "eks_cluster_endpoint" {
  value = module.eks.eks_cluster_endpoint
}

output "node_instance_role" {
  value = module.nodes.node_instance_role.arn
}

output "eks_kubectl_role" {
  value = module.kubeconfig.eks_kubectl_role.arn
}
